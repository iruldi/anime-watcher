import { useStorageLocal } from '~/composables/useStorageLocal'

export const animeList = useStorageLocal('anime-list', '[]')
